# WaSQL - Web access to SQL

## What is WaSQL?
WaSQL is web application platform currently written in PHP with a few supporting files written in Perl.  It is designed to help you build web sites, forms, e-commerce, intranet and other custom web applications rapidly.  WaSQL deploys pages and applications using a database driven MVC architecture.  It is a stand-alone platform as it does not require any outside or 3rd party add-ons to work.  It is also a replacement for PhpMyAdmin since database schema management is built in.  User management is also built in.

## WaSQL License
WaSQL is free for both personal and business use. Read the full license [here](license.md)

## Required Skills
To use WaSQL effectively you need to know HTML5, CSS3, JavaScript SQL, and PHP.  Many functions are already built for you but you need to understand programming logic to really use it.

## Best Way to Learn WaSQL
I have found that the best way to learn WaSQL is to download it and use it.  I also recommend reading the functions found in database.php and common.php for starters.

## Where to Get Help
If you need professional help with you project please contact me at steve.lloyd@gmail.com.  There are also developers at http://www.devmavin.com that know WaSQL well and have used it often.

## How can I Help WaSQL become better?
Feel free to request changes via github.  You can also help by donating to the cause.  Donations can be sent via PayPal to steve.lloyd@gmail.com
